

import axios from 'axios';
import express from 'express';
import bodyParser from 'body-parser';
import moment from 'moment';



const app = express();
const port = 3000;

// Mock du service interne pour les horaires d'ouverture et les réservations
const serviceInterne = {
  getHorairesOuverture: () => ({
    ouverture: '08:00',
    fermeture: '18:00',
  }),
  getReservations: () => ({
    reservations: [
      { id: 1, salle: 'A', debut: '2023-07-21T09:00:00', fin: '2023-07-21T12:00:00' },
      { id: 2, salle: 'B', debut: '2023-07-21T14:00:00', fin: '2023-07-21T16:00:00' },
      { id: 1337, salle: 'C', debut: '2023-07-21T10:00:00', fin: '2023-07-21T11:00:00' },
    ],
  }),
};

app.use(bodyParser.json());

// Middleware pour valider les paramètres d'entrée
function validateInput(req: any, res: any, next: any) {
    const { id, datetime } = req.body;
  
    if (!id || typeof id !== 'number' || !datetime || !moment(datetime).isValid()) {
      return res.status(400).json({
        error: 'Les paramètres d\'entrée sont invalides.',
      });
    }
  
    next();
  }
  

// Endpoint pour vérifier la disponibilité de la salle
app.get('/verifier-disponibilite', validateInput, async (req, res) => {
  const { id, datetime } = req.body;

  // Vérifier si la salle est disponible en fonction des horaires d'ouverture et des réservations
  const horairesOuverture = serviceInterne.getHorairesOuverture();
  const reservations = serviceInterne.getReservations();

  const datetimeObj = moment(datetime);
  const heureOuverture = moment(horairesOuverture.ouverture, 'HH:mm');
  const heureFermeture = moment(horairesOuverture.fermeture, 'HH:mm');

  if (
    datetimeObj.isBefore(heureOuverture) ||
    datetimeObj.isAfter(heureFermeture)
  ) {
    return res.json({
      available: false,
    });
  }

  for (const reservation of reservations.reservations) {
    if (reservation.id === id && datetimeObj.isBetween(moment(reservation.debut), moment(reservation.fin))) {
      return res.json({
        available: false,
      });
    }
  }

  // La salle est disponible aux horaires d'ouverture et n'est pas réservée, on effectue la requête vers l'API cible

  try {
    const response = await axios.get(`http://localhost:8080/reservations?date=${datetime}&resourceId=${id}`);
    // Vérifiez la réponse de l'API cible pour déterminer si la salle est disponible selon ses propres règles
    // Vous pouvez ajouter une logique supplémentaire ici en fonction de la réponse reçue
    const apiResponseData = response.data;

    // Retournez la réponse en fonction de la réponse de l'API cible
    if (apiResponseData.reservations && apiResponseData.reservations.length > 0) {
      return res.json({
        available: false,
      });
    } else {
      return res.json({
        available: true,
      });
    }
  } catch (error) {
    // Gérer les erreurs d'appel à l'API cible
    return res.status(500).json({
      error: 'Erreur lors de la communication avec l\'API cible.',
    });
  }
});

app.listen(port, () => {
  console.log(`Le serveur est en écoute sur le port ${port}.`);
});
